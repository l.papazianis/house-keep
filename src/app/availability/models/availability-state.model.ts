import { EntityState } from '@ngrx/entity';
import { AvailabilityDayDto } from './availability-dto.model';

export type AvailabilityState = EntityState<AvailabilityDayDto>;
