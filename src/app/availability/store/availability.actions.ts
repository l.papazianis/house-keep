import { AvailabilityDto } from './../models/availability-dto.model';
import { Action } from '@ngrx/store';

export enum AvailabilityActionTypes {
    GET_AVAILABILITY = '[AVAILABILITY] Get Availability',
    SAVE_AVAILABILITY = '[AVAILABILITY] Save Availability'
}

export class GetAvailability implements Action {
    public readonly type = AvailabilityActionTypes.GET_AVAILABILITY;
}

export class SaveAvailability implements Action {
    public readonly type = AvailabilityActionTypes.SAVE_AVAILABILITY;
    constructor(public payload: AvailabilityDto) {}
}

export type AvailabilityActions = GetAvailability | SaveAvailability;
