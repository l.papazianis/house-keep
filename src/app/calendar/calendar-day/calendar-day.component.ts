import { Component, EventEmitter, Input, Output } from '@angular/core';
import { AvailabilityDay } from '../../availability/models/availability-day.model';
import { Booking } from '../../booking/models/booking-dto.model';
import { AvailabilitySlot } from '../../availability/models/availability-slot.model';

@Component({
    selector: 'app-calendar-day',
    templateUrl: './calendar-day.component.html',
    styleUrls: ['./calendar-day.component.less']
})
export class CalendarDayComponent {
    @Input()
    public day: AvailabilityDay;

    @Output()
    public onBookSlot: EventEmitter<Booking> = new EventEmitter<Booking>();

    book(slot: AvailabilitySlot) {
        this.onBookSlot.emit({
            date: this.day.date,
            start: slot.start,
            end: slot.end
        });
    }
}
