# HouseKeep

This project was generated with [Angular CLI](https://github.com/angular/angular-cli). It requires latest nodejs.
The app itself is a booking system that given  duration present the user with available cleaning slots that is able to book.
This is the repository that contains the requirements for this coding challenge (https://github.com/HousekeepLtd/coding-challenge)

## Api server

Run `npm run api:server` for a dev server. The dev server rungs on port 9000

## Local instance

Run `npm run start:local` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Tests.
Run `npm test`

Tests are not covering all the app, I tried though to showcase a sensible way of testing complex asynchronous streams(effects) by building workflows
and stubbing out services that provide observables with stubs that utilize behaviour subjects.

## Architecture

There is a mix of different patterns(repository, command etc). I tried to achieve an easy to follow, self explanatory architecture. 
There are three main modules that provide store capabilities and services(Availability and Booking both are using ngrx entity).
There is a presentational module that provides Calendar ui components. Finally there is a lazy loaded module that contains the 
container component that orchestrates via services the functionality. 
There are some abstractions to generalise the entity repositories but also the subscription handling in components. 
Effects are dealing with async logic. Services do the heavy lifting maintaining though single responsibility.

An interesting pattern is utilizing the dependency injection container of Angular and providing runtime abstract classes as Symbols and substitute
them depending on the environment or situation. This pattern makes it extremely easy to stub out services during testing or even provide a different
service depending on the environment.

## Improvements

I would write more tests. I didn't really have time to increase the coverage to an acceptable level. Fix the CI of gitlab with. I would use sass
for the css 

