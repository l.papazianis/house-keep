import { AvailabilitySlot } from './availability-slot.model';

export interface AvailabilityDay {
    date: Date;
    availabilitySlots: AvailabilitySlot[];
}
