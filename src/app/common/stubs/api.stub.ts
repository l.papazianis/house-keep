import {BehaviorSubject} from 'rxjs';

export abstract class ApiStub {
  private $state = new BehaviorSubject<any>('');
  protected happyPath = true;

  public shouldSucceed() {
    this.happyPath = true;
  }
  public shouldFail() {
    this.happyPath = false;
  }
  protected getState() {
    return this.$state.asObservable();
  }
  public setState(state) {
    this.$state.next(state);
    return this;
  }
}
