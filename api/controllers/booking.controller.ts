import { Request, Response } from 'express';
import { postMock } from '../mocks/booking.mock';

export function createBooking(req: Request, res: Response) {
    return res
        .status(200)
        .json(postMock(req.body))
        .end();
}
