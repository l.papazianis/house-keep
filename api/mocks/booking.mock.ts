import { DatePipe } from '@angular/common';
const datePipe = new DatePipe('en');

export const postMock = (requestBody: { date: string; start: string; end: string }) => {
    const date = datePipe.transform(new Date(requestBody.date), 'y-MM-dd');
    const start = datePipe.transform(new Date(requestBody.start), 'HH:mm');
    const end = datePipe.transform(new Date(requestBody.end), 'HH:mm');
    return {
        cleanerName: 'Jason',
        price: '25',
        currency: 'GBP',
        id: `${date}|${start}|${end}`
    };
};
