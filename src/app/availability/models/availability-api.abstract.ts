import { Observable } from 'rxjs';
import { AvailabilityDto } from './availability-dto.model';

export abstract class IAvailabilityApi {
    abstract getAvailability(): Observable<AvailabilityDto>;
}
