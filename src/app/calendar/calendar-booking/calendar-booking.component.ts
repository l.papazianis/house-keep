import { Component, Input, OnInit } from '@angular/core';
import { BookingRepository } from '../../booking/services/booking-repository.service';
import { Observable } from 'rxjs';
import { BookingDto } from '../../booking/models/booking-dto.model';
import { filter } from 'rxjs/operators';

@Component({
    selector: 'app-calendar-booking',
    templateUrl: './calendar-booking.component.html',
    styleUrls: ['./calendar-booking.component.less']
})
export class CalendarBookingComponent implements OnInit {
    @Input()
    public bookingId: string;

    public booking$: Observable<BookingDto>;

    constructor(private bookingRepository: BookingRepository) {}

    ngOnInit() {
        this.booking$ = this.bookingRepository.getItemById$(this.bookingId).pipe(filter(Boolean));
    }
}
