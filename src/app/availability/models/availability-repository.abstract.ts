import { Observable } from 'rxjs';
import { AvailabilityDay } from './availability-day.model';

export abstract class IAvailabilityRepository {
    abstract getAvailability(duration?: number): Observable<AvailabilityDay[]>;
}
