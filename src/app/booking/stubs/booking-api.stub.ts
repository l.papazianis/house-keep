import {ApiStub} from '../../common/stubs/api.stub';
import {IBookingApi} from '../models/booking-api.abstract';
import { Booking } from '../models/booking-dto.model';
import { throwError } from 'rxjs';

export class BookingApiStub extends ApiStub implements IBookingApi {
  book(booking: Booking) {
    return this.happyPath ? this.getState() : throwError({});
  }
}
