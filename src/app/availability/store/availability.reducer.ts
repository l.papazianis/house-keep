import { AvailabilityActions, AvailabilityActionTypes } from './availability.actions';
import { AvailabilityDayDto } from './../models/availability-dto.model';
import { EntityAdapter, createEntityAdapter } from '@ngrx/entity';

export const availabilityAdapter: EntityAdapter<AvailabilityDayDto> = createEntityAdapter<
    AvailabilityDayDto
>({
    selectId: (entity: AvailabilityDayDto) => entity.day
});

export const availabilityInitialState = availabilityAdapter.getInitialState();
export function availabilityReducer(state = availabilityInitialState, action: AvailabilityActions) {
    switch (action.type) {
        case AvailabilityActionTypes.SAVE_AVAILABILITY:
            return availabilityAdapter.upsertMany(action.payload.days, state);
        default:
            return state;
    }
}
