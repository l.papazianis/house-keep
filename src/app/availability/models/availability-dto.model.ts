export interface AvailabilityDto {
    weekBeginning: string;
    days: AvailabilityDayDto[];
}
export interface AvailabilityDayDto {
    day: string;
    availability: AvailabilitySlotDto[];
}
export interface AvailabilitySlotDto {
    start: string;
    end: string;
}
