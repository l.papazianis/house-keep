import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {TestBed} from '@angular/core/testing';
import {EffectsModule, Actions} from '@ngrx/effects';
import {StoreModule} from '@ngrx/store';
import {AvailabilityModule} from '../../availability/availability.module';
import {IBookingApi} from '../models/booking-api.abstract';
import {BookingApi} from '../services/booking-api.service';
import {BookingModule} from '../booking.module';
import {BookingUrls} from '../models/booking-urls.enum';

describe('BookingApi', () => {
  let http: HttpTestingController;
  let api: BookingApi;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [Actions],
      imports: [
        EffectsModule.forRoot([]),
        StoreModule.forRoot({}),
        AvailabilityModule,
        BookingModule,
        HttpClientTestingModule
      ]
    });
    http = TestBed.get(HttpTestingController);
    api = TestBed.get(IBookingApi);
  });
  describe('Book method', () => {
    it('Should call the booking api resource with the appropriate payload', () => {
      const booking = {date: new Date(), end: new Date(), start: new Date()};
      api.book(booking).subscribe();
      http.expectOne({
        url: BookingUrls.resource,
        method: 'POST',
      });
    });
  });
});
