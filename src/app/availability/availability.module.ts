import { AvailabilityRepository } from './services/availability-repository.service';
import { AvailabilityEvents } from './services/availability-events.service';
import { AvailabilityDtoTranslator } from './services/availability-dto-translator.service';
import { AvailabilityApi } from './services/availability-api.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { availabilityReducer } from './store/availability.reducer';
import { EffectsModule } from '@ngrx/effects';
import { AvailabilityEffects } from './store/availability.effects';
import { IAvailabilityApi } from './models/availability-api.abstract';
import { IAvailabilityRepository } from './models/availability-repository.abstract';

@NgModule({
    imports: [
        CommonModule,
        StoreModule.forFeature('availability', availabilityReducer),
        EffectsModule.forFeature([AvailabilityEffects])
    ],
    providers: [
        {
            provide: IAvailabilityApi,
            useClass: AvailabilityApi
        },
        {
            provide: IAvailabilityRepository,
            useClass: AvailabilityRepository
        },
        AvailabilityDtoTranslator,
        AvailabilityEvents
    ],
    declarations: []
})
export class AvailabilityModule {}
