import { Subscription } from 'rxjs';

export class SubscriptionManager {
    private subscriptions: Subscription[] = [];
    public add(subscription: Subscription) {
        this.subscriptions = [...this.subscriptions, subscription];
    }
    public clear() {
        this.subscriptions.forEach(
            (subscription: Subscription) =>
                subscription.closed ? undefined : subscription.unsubscribe()
        );
        this.subscriptions = [];
    }
}
