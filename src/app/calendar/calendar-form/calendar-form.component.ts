import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { debounceTime } from 'rxjs/operators';
import { SubscriptionManager } from '../../common/models/subscription-manager';

interface CalendarForm {
    duration: string;
}

@Component({
    selector: 'app-calendar-form',
    templateUrl: './calendar-form.component.html',
    styleUrls: ['./calendar-form.component.less']
})
export class CalendarFormComponent implements OnInit, OnDestroy {
    @Output()
    onFormChange = new EventEmitter<string | undefined>();

    public form: FormGroup;

    private subscriptionManager = new SubscriptionManager();

    ngOnInit() {
        this.form = new FormGroup({
            duration: new FormControl('')
        });
        const subscription = this.form.valueChanges
            .pipe(debounceTime(250))
            .subscribe((form: CalendarForm) => this.onFormChange.emit(form.duration));
        this.subscriptionManager.add(subscription);
    }
    ngOnDestroy() {
        this.subscriptionManager.clear();
    }
}
