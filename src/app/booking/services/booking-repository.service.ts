import { Injectable } from '@angular/core';
import { EntityRepository } from '../../common/models/entity-repository';
import { BookingDto } from '../models/booking-dto.model';
import { BookingState } from '../models/booking-state.model';
import { Store } from '@ngrx/store';
import { bookingAdapter } from '../store/booking.reducer';
import { getBookingState } from '../store/booking.selectors';
import { IBookingRepository } from '../models/booking-repository.abstract';

@Injectable()
export class BookingRepository extends EntityRepository<BookingDto> implements IBookingRepository {
    constructor(store$: Store<BookingState>) {
        super(store$, bookingAdapter.getSelectors(), getBookingState);
    }
}
