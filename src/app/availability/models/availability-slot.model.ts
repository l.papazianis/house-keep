export interface AvailabilitySlot {
    duration: number;
    start: Date;
    end: Date;
    available: boolean;
    bookingId: string;
    booked?: boolean;
}
