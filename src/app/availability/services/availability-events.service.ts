import { AvailabilityDto } from './../models/availability-dto.model';
import {
    GetAvailability,
    SaveAvailability,
    AvailabilityActionTypes
} from './../store/availability.actions';
import { Observable } from 'rxjs';
import { AvailabilityState } from './../models/availability-state.model';
import { Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { Injectable } from '@angular/core';

@Injectable()
export class AvailabilityEvents {
    constructor(private actions$: Actions, private store$: Store<AvailabilityState>) {}
    fetch(): this {
        this.store$.dispatch(new GetAvailability());
        return this;
    }
    save(availability: AvailabilityDto): this {
        this.store$.dispatch(new SaveAvailability(availability));
        return this;
    }
    onGet(): Observable<GetAvailability> {
        return this.actions$.pipe(ofType(AvailabilityActionTypes.GET_AVAILABILITY));
    }
}
