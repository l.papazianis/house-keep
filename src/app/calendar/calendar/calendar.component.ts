import { Component, EventEmitter, Input, Output } from '@angular/core';
import { AvailabilityDay } from '../../availability/models/availability-day.model';
import { Booking } from '../../booking/models/booking-dto.model';

@Component({
    selector: 'app-calendar',
    templateUrl: './calendar.component.html',
    styleUrls: ['./calendar.component.less']
})
export class CalendarComponent {
    @Input()
    public availability: AvailabilityDay[];

    @Output()
    public onBookSlot: EventEmitter<Booking> = new EventEmitter<Booking>();

    book(booking: Booking) {
        this.onBookSlot.emit(booking);
    }
}
