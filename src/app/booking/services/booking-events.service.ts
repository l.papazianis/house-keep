import { Injectable } from '@angular/core';
import { BookingState } from '../models/booking-state.model';
import { Store } from '@ngrx/store';
import { Actions, ofType } from '@ngrx/effects';
import { Booking, BookingDto } from '../models/booking-dto.model';
import { Book, Booked, BookingActionTypes } from '../store/booking.actions';
import { Observable } from 'rxjs';
import { pluck } from 'rxjs/operators';

@Injectable()
export class BookingEvents {
    constructor(private store$: Store<BookingState>, private actions$: Actions) {}

    book(booking: Booking): this {
        this.store$.dispatch(new Book(booking));
        return this;
    }
    booked(booking: BookingDto): this {
        this.store$.dispatch(new Booked(booking));
        return this;
    }
    onBooked(): Observable<Booked> {
        return this.actions$.pipe(ofType(BookingActionTypes.BOOKED));
    }
    onBook(): Observable<Booking> {
        return this.actions$.pipe(
            ofType(BookingActionTypes.BOOK),
            pluck('payload')
        );
    }
}
