import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AvailabilityDto } from '../models/availability-dto.model';
import { Observable } from 'rxjs';
import { AvailabilityUrls } from '../models/availability-url.enums';
import { IAvailabilityApi } from '../models/availability-api.abstract';

@Injectable()
export class AvailabilityApi implements IAvailabilityApi {
    constructor(private http: HttpClient) {}

    getAvailability(): Observable<AvailabilityDto> {
        return this.http.get<AvailabilityDto>(AvailabilityUrls.resource);
    }
}
