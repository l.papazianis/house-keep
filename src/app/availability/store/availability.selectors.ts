import { AvailabilityState } from './../models/availability-state.model';
import { MemoizedSelector, createFeatureSelector } from '@ngrx/store';

export const getAvailabilityState: MemoizedSelector<
    object,
    AvailabilityState
> = createFeatureSelector('availability');
