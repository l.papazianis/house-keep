import { EntityState } from '@ngrx/entity';
import { Observable } from 'rxjs';
import { Store, MemoizedSelector, createSelector, select } from '@ngrx/store';
import { EntitySelectors, Dictionary } from '@ngrx/entity/src/models';

export class EntityRepository<T> {
    constructor(
        protected store: Store<EntityState<T>>,
        protected selectors: EntitySelectors<T, EntityState<T>>,
        protected rootStateSelector: MemoizedSelector<object, EntityState<T>>
    ) {}
    public getIds$(): Observable<string[] | number[]> {
        return this.store.pipe(
            select(createSelector(this.rootStateSelector, this.selectors.selectIds))
        );
    }
    public getEntities$(): Observable<Dictionary<T>> {
        return this.store.pipe(
            select(createSelector(this.rootStateSelector, this.selectors.selectEntities))
        );
    }
    public getAll$(): Observable<T[]> {
        return this.store.pipe(
            select(createSelector(this.rootStateSelector, this.selectors.selectAll))
        );
    }
    public getItemById$(id: string): Observable<T | undefined> {
        return this.store.pipe(
            select(
                createSelector(
                    this.rootStateSelector,
                    (state: EntityState<T>) => state.entities[id]
                )
            )
        );
    }
}
