import { Component, OnInit } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { AvailabilityDay } from '../../../availability/models/availability-day.model';
import { CalendarPageService } from '../../services/calendar-page.service';
import { Booking } from '../../../booking/models/booking-dto.model';

@Component({
    selector: 'app-calendar-page',
    templateUrl: './calendar-page.component.html',
    styleUrls: ['./calendar-page.component.less']
})
export class CalendarPageComponent implements OnInit {
    public availability$: Observable<AvailabilityDay[]>;

    private duration$ = new BehaviorSubject('');

    constructor(private calendarPageServive: CalendarPageService) {}

    ngOnInit() {
        this.calendarPageServive.initialize();
        this.availability$ = this.calendarPageServive.getAvailability(
            this.duration$.asObservable()
        );
    }

    onFormChange(duration: string | undefined) {
        this.duration$.next(duration);
    }

    book(booking: Booking) {
        this.calendarPageServive.book(booking);
    }
}
