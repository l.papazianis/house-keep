import { Request, Response } from 'express';
import { getMock } from '../mocks/availability.mock';

export function getAvailability(req: Request, res: Response) {
    return res
        .status(200)
        .json(getMock)
        .end();
}
