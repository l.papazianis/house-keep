import {Observable} from 'rxjs';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {AvailabilityRepository} from '../../availability/services/availability-repository.service';
import {TestBed} from '@angular/core/testing';
import {provideMockActions} from '@ngrx/effects/testing';
import {EffectsModule} from '@ngrx/effects';
import {StoreModule} from '@ngrx/store';
import {AvailabilityModule} from '../../availability/availability.module';
import {IBookingApi} from '../models/booking-api.abstract';
import {IBookingRepository} from '../models/booking-repository.abstract';
import {BookingEffects} from './booking.effects';
import {BookingModule} from '../booking.module';
import {cold, hot} from 'jasmine-marbles';
import {Book} from './booking.actions';
import {BookingApiStub} from '../stubs/booking-api.stub';
import {BookingDto} from '../models/booking-dto.model';
import {mergeMap} from 'rxjs/operators';

describe('BookingEffects', () => {
  let effects: BookingEffects;
  let actions$: Observable<any>;
  let http: HttpTestingController;
  let bookingRepository: AvailabilityRepository;
  let api: BookingApiStub =  new BookingApiStub();

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideMockActions(() => actions$), {
        provide: IBookingApi,
        useValue: api
      }],
      imports: [
        EffectsModule.forRoot([]),
        StoreModule.forRoot({}),
        AvailabilityModule,
        BookingModule,
        HttpClientTestingModule
      ]
    });
    http = TestBed.get(HttpTestingController);
    effects = TestBed.get(BookingEffects);
    bookingRepository = TestBed.get(IBookingRepository);
    api = TestBed.get(IBookingApi);
  });
  describe('When a book action is dispatched and the api call for the booking is successful', () => {
    it('Should store the booking in the store', () => {
      const booking = new Book({date: new Date(), end: new Date(), start: new Date()});
      const response: BookingDto = { cleanerName: 'Joe', id: '1', currency: 'EUR', price: '10.00' };
      api.setState(response);
      api.shouldSucceed();
      actions$ = hot('--a', { a:  booking});
      const workflow = effects.onBook$.pipe(mergeMap(() => bookingRepository.getAll$()));
      expect(workflow).toBeObservable(cold('--b', {b: [response]}));
    });
  });
});
