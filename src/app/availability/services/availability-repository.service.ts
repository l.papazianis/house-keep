import { getAvailabilityState } from './../store/availability.selectors';
import { AvailabilityDtoTranslator } from './availability-dto-translator.service';
import { Store } from '@ngrx/store';
import { AvailabilityDayDto } from './../models/availability-dto.model';
import { EntityRepository } from '../../common/models/entity-repository';
import { AvailabilityState } from '../models/availability-state.model';
import { map } from 'rxjs/operators';
import { availabilityAdapter } from '../store/availability.reducer';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AvailabilityDay } from '../models/availability-day.model';
import { IAvailabilityRepository } from '../models/availability-repository.abstract';

@Injectable()
export class AvailabilityRepository extends EntityRepository<AvailabilityDayDto>
    implements IAvailabilityRepository {
    constructor(
        store$: Store<AvailabilityState>,
        private availabilityDtoTranslator: AvailabilityDtoTranslator
    ) {
        super(store$, availabilityAdapter.getSelectors(), getAvailabilityState);
    }
    getAvailability(duration?: number): Observable<AvailabilityDay[]> {
        return this.getAll$().pipe(
            map((items: AvailabilityDayDto[]) => {
                return items.map(this.availabilityDtoTranslator.translate(duration));
            })
        );
    }
}
