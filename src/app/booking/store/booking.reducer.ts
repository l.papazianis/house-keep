import { createEntityAdapter, EntityAdapter } from '@ngrx/entity';
import { BookingDto } from '../models/booking-dto.model';
import { BookingActions, BookingActionTypes } from './booking.actions';

export const bookingAdapter: EntityAdapter<BookingDto> = createEntityAdapter<BookingDto>({
    selectId: (entity: BookingDto) => entity.id
});

export const bookingInitialState = bookingAdapter.getInitialState();

export function bookingReducer(state = bookingInitialState, action: BookingActions) {
    switch (action.type) {
        case BookingActionTypes.BOOKED:
            return bookingAdapter.upsertOne(action.payload, state);
        default:
            return state;
    }
}
