import { createFeatureSelector, MemoizedSelector } from '@ngrx/store';
import { BookingState } from '../models/booking-state.model';

export const getBookingState: MemoizedSelector<object, BookingState> = createFeatureSelector(
    'bookings'
);
