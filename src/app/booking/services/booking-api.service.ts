import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Booking, BookingDto } from '../models/booking-dto.model';
import { BookingUrls } from '../models/booking-urls.enum';
import { Observable } from 'rxjs';
import { IBookingApi } from '../models/booking-api.abstract';

@Injectable()
export class BookingApi implements IBookingApi {
    constructor(private http: HttpClient) {}

    book(payload: Booking): Observable<BookingDto> {
        return this.http.post<BookingDto>(BookingUrls.resource, payload);
    }
}
