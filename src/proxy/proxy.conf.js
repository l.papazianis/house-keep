const createProxy = (target) => [
  {
    context: [
      "/api/**/*"
    ],
    target,
    secure: false
  }
];

module.exports = createProxy;
