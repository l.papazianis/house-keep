import { defaults } from './env.defaults';

export const environment = {
    production: false,
    name: 'dev',
    ...defaults
};
