import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AvailabilitySlot } from '../../availability/models/availability-slot.model';
import { BookingRepository } from '../../booking/services/booking-repository.service';
import { Observable } from 'rxjs';
import { BookingDto } from '../../booking/models/booking-dto.model';
import {IBookingRepository} from '../../booking/models/booking-repository.abstract';

@Component({
    selector: 'app-calendar-availability-slot',
    templateUrl: './calendar-availability-slot.component.html',
    styleUrls: ['./calendar-availability-slot.component.less']
})
export class CalendarAvailabilitySlotComponent implements OnInit {
    @Input()
    public slot: AvailabilitySlot;

    @Output()
    public onBookSlot: EventEmitter<AvailabilitySlot> = new EventEmitter<AvailabilitySlot>();

    public booking$: Observable<BookingDto | undefined>;

    constructor(private bookingRepository: IBookingRepository) {}

    ngOnInit() {
        this.booking$ = (this.bookingRepository as BookingRepository).getItemById$(this.slot.bookingId);
    }

    getTitle() {
        return `${this.slot.duration} ${this.slot.duration === 1 ? 'Hour' : 'Hours'}`;
    }

    book() {
        if (this.slot.available) {
            this.onBookSlot.emit(this.slot);
        }
    }
}
