import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalendarComponent } from './calendar/calendar.component';
import { CalendarBookingComponent } from './calendar-booking/calendar-booking.component';
import { CalendarDayComponent } from './calendar-day/calendar-day.component';
import { CalendarFormComponent } from './calendar-form/calendar-form.component';
import { CalendarAvailabilitySlotComponent } from './calendar-availability-slot/calendar-availability-slot.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
    imports: [CommonModule, ReactiveFormsModule],
    declarations: [
        CalendarComponent,
        CalendarBookingComponent,
        CalendarDayComponent,
        CalendarFormComponent,
        CalendarAvailabilitySlotComponent
    ],
    exports: [CalendarFormComponent, CalendarComponent]
})
export class CalendarModule {}
