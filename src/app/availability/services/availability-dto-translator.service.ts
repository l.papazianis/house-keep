import { AvailabilityDayDto, AvailabilitySlotDto } from './../models/availability-dto.model';
import { AvailabilityDay } from './../models/availability-day.model';
import { Injectable } from '@angular/core';
import { DatePipe } from '@angular/common';
const datePipe = new DatePipe('en');

@Injectable()
export class AvailabilityDtoTranslator {
    translate = (requiredDuration?: number) => (item: AvailabilityDayDto): AvailabilityDay => {
        const date = new Date(item.day);
        return {
            date,
            availabilitySlots: item.availability.map((_: AvailabilitySlotDto) => {
                const start = new Date(`${item.day}T${_.start}`);
                const end = new Date(`${item.day}T${_.end}`);
                const duration = (end.getTime() - start.getTime()) / 36e5;
                const formattedDate = datePipe.transform(date, 'y-MM-dd');
                const bookingId = `${formattedDate}|${_.start}|${_.end}`;
                return {
                    start,
                    end,
                    duration,
                    bookingId,
                    available: requiredDuration ? requiredDuration === duration : true,
                    booked: false
                };
            })
        };
    };
}
