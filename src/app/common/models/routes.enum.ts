export enum RoutesEnum {
    Default = '**',
    HouseKeep = 'house-keep',
    Calendar = 'calendar'
}
