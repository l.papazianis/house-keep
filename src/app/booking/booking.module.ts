import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookingApi } from './services/booking-api.service';
import { BookingEvents } from './services/booking-events.service';
import { BookingRepository } from './services/booking-repository.service';
import { StoreModule } from '@ngrx/store';
import { bookingReducer } from './store/booking.reducer';
import { EffectsModule } from '@ngrx/effects';
import { BookingEffects } from './store/booking.effects';
import { IBookingRepository } from './models/booking-repository.abstract';
import { IBookingApi } from './models/booking-api.abstract';

@NgModule({
    imports: [
        CommonModule,
        StoreModule.forFeature('bookings', bookingReducer),
        EffectsModule.forFeature([BookingEffects])
    ],
    providers: [
        {
            provide: IBookingApi,
            useClass: BookingApi
        },
        {
            provide: IBookingRepository,
            useClass: BookingRepository
        },
        BookingEvents
    ],
    declarations: []
})
export class BookingModule {}
