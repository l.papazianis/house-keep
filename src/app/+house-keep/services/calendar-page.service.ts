import { Injectable } from '@angular/core';
import { AvailabilityEvents } from '../../availability/services/availability-events.service';
import { BookingEvents } from '../../booking/services/booking-events.service';
import { combineLatest, Observable } from 'rxjs';
import { AvailabilityDay } from '../../availability/models/availability-day.model';
import { map, switchMap } from 'rxjs/operators';
import { Dictionary } from '@ngrx/entity';
import { Booking, BookingDto } from '../../booking/models/booking-dto.model';
import { AvailabilitySlot } from '../../availability/models/availability-slot.model';
import { IAvailabilityRepository } from '../../availability/models/availability-repository.abstract';
import { IBookingRepository } from '../../booking/models/booking-repository.abstract';
import { BookingRepository } from '../../booking/services/booking-repository.service';

@Injectable()
export class CalendarPageService {
    constructor(
        private bookingsRepository: IBookingRepository,
        private availabilityRepository: IAvailabilityRepository,
        private availabilityEvents: AvailabilityEvents,
        private bookingEvents: BookingEvents
    ) {}

    initialize() {
        this.availabilityEvents.fetch();
    }

    getAvailability(duration$: Observable<string | undefined>): Observable<AvailabilityDay[]> {
        const toDayWithBookings = (bookings: Dictionary<BookingDto>) => (day: AvailabilityDay) => ({
            ...day,
            availabilitySlots: day.availabilitySlots.map((slot: AvailabilitySlot) => ({
                ...slot,
                booked: !!bookings[slot.bookingId]
            }))
        });
        const toAvailabilityDays = ([duration, bookings]: [
            string | undefined,
            Dictionary<BookingDto>
        ]) =>
            this.availabilityRepository
                .getAvailability(Number(duration))
                .pipe(map((days: AvailabilityDay[]) => days.map(toDayWithBookings(bookings))));
        return combineLatest(
            duration$,
            (this.bookingsRepository as BookingRepository).getEntities$()
        ).pipe(switchMap(toAvailabilityDays));
    }
    book(booking: Booking) {
        this.bookingEvents.book(booking);
    }
}
