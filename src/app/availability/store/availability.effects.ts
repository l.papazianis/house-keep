import { AvailabilityDto } from './../models/availability-dto.model';
import { AvailabilityEvents } from './../services/availability-events.service';
import { Effect } from '@ngrx/effects';
import { switchMap, tap } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { IAvailabilityApi } from '../models/availability-api.abstract';

@Injectable()
export class AvailabilityEffects {
    @Effect({ dispatch: false })
    public onGetAvailability$ = this.onGetAvailability();

    constructor(
        private availabilityEvents: AvailabilityEvents,
        private availabilityApi: IAvailabilityApi
    ) {}
    private onGetAvailability() {
        return this.availabilityEvents
            .onGet()
            .pipe(
                switchMap(() =>
                    this.availabilityApi
                        .getAvailability()
                        .pipe(
                            tap((response: AvailabilityDto) =>
                                this.availabilityEvents.save(response)
                            )
                        )
                )
            );
    }
}
