import { Booking, BookingDto } from './booking-dto.model';
import { Observable } from 'rxjs';

export abstract class IBookingApi {
    abstract book(payload: Booking): Observable<BookingDto>;
}
