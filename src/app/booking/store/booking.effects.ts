import { Injectable } from '@angular/core';
import { Effect } from '@ngrx/effects';
import { BookingEvents } from '../services/booking-events.service';
import { switchMap, tap } from 'rxjs/operators';
import { Booking, BookingDto } from '../models/booking-dto.model';
import { IBookingApi } from '../models/booking-api.abstract';

@Injectable()
export class BookingEffects {
    @Effect({ dispatch: false })
    public onBook$ = this.onBook();

    constructor(private bookingEvents: BookingEvents, private bookingApi: IBookingApi) {}

    private onBook() {
        return this.bookingEvents
            .onBook()
            .pipe(
                switchMap((booking: Booking) =>
                    this.bookingApi
                        .book(booking)
                        .pipe(tap((response: BookingDto) => this.bookingEvents.booked(response)))
                )
            );
    }
}
