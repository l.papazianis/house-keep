import { EntityState } from '@ngrx/entity';
import { BookingDto } from './booking-dto.model';

export type BookingState = EntityState<BookingDto>;
