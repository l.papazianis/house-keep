import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CalendarPageComponent } from './containers/calendar-page/calendar-page.component';
import { RouterModule } from '@angular/router';
import { AvailabilityModule } from '../availability/availability.module';
import { BookingModule } from '../booking/booking.module';
import { CalendarModule } from '../calendar/calendar.module';
import { CalendarPageService } from './services/calendar-page.service';
import { routes } from './routes';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        AvailabilityModule,
        BookingModule,
        CalendarModule
    ],
    providers: [CalendarPageService],
    declarations: [CalendarPageComponent]
})
export class HouseKeepModule {}
