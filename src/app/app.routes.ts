import { Route, Routes } from '@angular/router';
import { RoutesEnum } from './common/models/routes.enum';

const houseKeepRoute: Route = {
    path: RoutesEnum.HouseKeep,
    loadChildren: './+house-keep/house-keep.module#HouseKeepModule'
};

const defaultRouteRedirect: Route = {
    path: RoutesEnum.Default,
    redirectTo: '/house-keep/calendar'
};

export const routes: Routes = [houseKeepRoute, defaultRouteRedirect];
