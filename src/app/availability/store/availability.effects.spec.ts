import { AvailabilityUrls } from './../models/availability-url.enums';
import { GetAvailability } from './availability.actions';
import { AvailabilityModule } from './../availability.module';
import { EffectsModule } from '@ngrx/effects';
import { TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { AvailabilityEffects } from './availability.effects';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { provideMockActions } from '@ngrx/effects/testing';
import { StoreModule } from '@ngrx/store';
import { cold, hot } from 'jasmine-marbles';
import { mergeMap } from 'rxjs/operators';
import { getMock } from '../../../../api/mocks/availability.mock';
import { IAvailabilityApi } from '../models/availability-api.abstract';
import { IAvailabilityRepository } from '../models/availability-repository.abstract';
import { AvailabilityRepository } from '../services/availability-repository.service';
import { AvailabilityApi } from '../services/availability-api.service';

describe('Availability Effects', () => {
    let effects: AvailabilityEffects;
    let actions$: Observable<any>;
    let http: HttpTestingController;
    let repository: AvailabilityRepository;
    let api: AvailabilityApi;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [provideMockActions(() => actions$)],
            imports: [
                EffectsModule.forRoot([]),
                StoreModule.forRoot({}),
                AvailabilityModule,
                HttpClientTestingModule
            ]
        });
        http = TestBed.get(HttpTestingController);
        effects = TestBed.get(AvailabilityEffects);
        repository = TestBed.get(IAvailabilityRepository);
        api = TestBed.get(IAvailabilityApi);
    });

    describe('When a get availability action is dispatched', () => {
        it('Should fetch the availability from the api', () => {
            actions$ = hot('--a', { a: new GetAvailability() });
            expect(effects.onGetAvailability$).toBeObservable(cold(''));
            http.expectOne({
                url: AvailabilityUrls.resource,
                method: 'GET'
            });
        });
        it('Should store the availability that fetched from the api into the store', () => {
            actions$ = hot('--a', { a: new GetAvailability() });
            const spy = spyOn(api, 'getAvailability').and.returnValue(of(getMock));
            const workflow = effects.onGetAvailability$.pipe(mergeMap(() => repository.getIds$()));
            expect(workflow).toBeObservable(cold('--b', { b: getMock.days.map(day => day.day) }));
            spy.calls.reset();
        });
    });
});
