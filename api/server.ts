import * as express from 'express';
import { NextFunction, Response, Request } from 'express';
import { AvailabilityUrls } from '../src/app/availability/models/availability-url.enums';
import { getAvailability } from './controllers/availability.controller';
import { BookingUrls } from '../src/app/booking/models/booking-urls.enum';
import { createBooking } from './controllers/booking.controller';

const createServer = () => {
    const app: express.Application = express();
    const PORT = 9000;

    app.use(express.json()).use(express.urlencoded());

    app.get(AvailabilityUrls.resource, getAvailability);

    app.post(BookingUrls.resource, createBooking);

    app.use((error: Error, req: Request, res: Response, next: NextFunction) => {
        if (!error) {
            next();
        } else {
            console.error(error);
            res.send(500);
        }
    });

    process.on('uncaughtException', console.error);

    app.listen(PORT, () => {
        console.log(`Listening at http://localhost:${PORT}`);
    }).on('error', console.error);
};

createServer();
