import { Action } from '@ngrx/store';
import { Booking, BookingDto } from '../models/booking-dto.model';

export enum BookingActionTypes {
    BOOK = '[BOOKING] Book',
    BOOKED = '[BOOKING] Booked'
}

export class Book implements Action {
    public readonly type = BookingActionTypes.BOOK;
    constructor(public payload: Booking) {}
}

export class Booked implements Action {
    public readonly type = BookingActionTypes.BOOKED;
    constructor(public payload: BookingDto) {}
}

export type BookingActions = Book | Booked;
