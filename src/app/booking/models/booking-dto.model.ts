export interface BookingDto {
    cleanerName: string;
    price: string;
    currency: string;
    id: string;
}
export interface Booking {
    date: Date;
    start: Date;
    end: Date;
}
