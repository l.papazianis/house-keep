import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { ActionReducer, META_REDUCERS, MetaReducer, StoreModule } from '@ngrx/store';
import { storeLogger } from 'ngrx-store-logger';
import { environment } from '../environments/environment';
import { storeFreeze } from 'ngrx-store-freeze';
import { RouterModule } from '@angular/router';
import { routes } from './app.routes';
import { EffectsModule } from '@ngrx/effects';
import { HttpClientModule } from '@angular/common/http';

export function logger(reducer: ActionReducer<any>) {
    return storeLogger({})(reducer);
}

export const devMetaReducers: MetaReducer<any>[] = [
    ...(environment.production ? [] : [storeFreeze, logger])
];
export function metaReducerFactory() {
    return [...devMetaReducers];
}
export const metaReducers = {
    provide: META_REDUCERS,
    useFactory: metaReducerFactory
};

@NgModule({
    declarations: [AppComponent],
    imports: [
        BrowserModule,
        RouterModule.forRoot(routes),
        StoreModule.forRoot({}),
        EffectsModule.forRoot([]),
        HttpClientModule
    ],
    providers: [metaReducers],
    bootstrap: [AppComponent]
})
export class AppModule {}
