import { Route } from '@angular/router';
import { RoutesEnum } from '../common/models/routes.enum';
import { CalendarPageComponent } from './containers/calendar-page/calendar-page.component';

const calendarPageRoute: Route = {
    path: RoutesEnum.Calendar,
    component: CalendarPageComponent,
    canActivate: []
};

export const routes = [calendarPageRoute];
